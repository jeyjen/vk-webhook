package polilingv

import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.application.Application
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import org.junit.Test

class VkWebhookTest{
    @Test
    fun confirmationEvent() = withTestApplication(Application::service) {
        val om = ObjectMapper()

        val event = om.createObjectNode()
        event.put("type", "confirmation")
        event.put("group_id", 1000001)
        val call = handleRequest(HttpMethod.Post, "/?code=somecode&version=92.58") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(event.toString())
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
    @Test
    fun messageNew() = withTestApplication(Application::service) {
        val om = ObjectMapper()
        val event = om.createObjectNode()
        event.put("type", "message_new")
        event.put("group_id", 1000001)
        val obj = event.putObject("object")
        obj.put("body", "hello botinok")
        obj.put("user_id", "321")
        val call = handleRequest(HttpMethod.Post, "/vk/qweeqw") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(event.toString())
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
    @Test
    fun messageAllow() = withTestApplication(Application::service) {
        val om = ObjectMapper()
        val event = om.createObjectNode()
        event.put("type", "message_allow")
        event.put("group_id", 1000001)
        val call = handleRequest(HttpMethod.Post, "/vk/qweeqw") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(event.toString())
        }
        with(call){
            var data = response.content
            println(data)
        }
    }
}

