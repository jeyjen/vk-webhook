package polilingv

import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.runBlocking
import org.junit.Test

class HandlerTest{
    val om = ObjectMapper()

    @Test
    fun confirmationEvent() = runBlocking {
        val handler = Handler()
        val event1 = om.createObjectNode()
        event1.put("type", "confirmation")
        event1.put("version", "2")
        event1.put("group_id", "123321")
        val list = listOf(event1)

        handler.handle(list)

        // запросить БД и проверить запись
    }

}

