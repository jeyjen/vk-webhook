package polilingv

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.client.HttpClient
import io.ktor.client.engine.config
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.content.TextContent
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel

interface Service<T>{
    suspend fun send(message: T)
}

class  Handler: Service<Event>{
    private val om = ObjectMapper()
    private val _client = HttpClient(io.ktor.client.engine.apache.Apache.config {
        customizeClient {}
    })
    private var _job :Job? = null

    val channel = Channel<Event>()

    fun start(){
        _job = GlobalScope.launch {
            var bundle = ArrayList<JsonNode>(1000)
            while (true){
                var count = 0
                while (! channel.isEmpty && ! channel.isClosedForReceive){
                    bundle.add(om.valueToTree(channel.receive()))
                    count++
                    if(count == bundle.size)
                        break
                }
                if(count > 0){
                    handle(bundle)
                    bundle = ArrayList(1000)
                }
                else{
                    delay(1000)
                }
            }
        }
        _job?.start()
    }
    fun stop(){
        _job?.cancel()
    }

    private suspend fun request(verb: HttpMethod, url: String, content: JsonNode? = null): JsonNode {
        val t = content?.toString()?:""
        var res = try {
            _client.request<String> {
                url(url)
                method = verb
                body = TextContent(t, contentType = ContentType.Application.Json)
            }
        } catch (e: Exception) {
            println(e.message)
            e.printStackTrace()
            "{}"
        }

        return om.readTree(res)
    }

    override suspend fun send(event: Event){
        channel.send(event)
    }

    suspend fun handle(objs: List<JsonNode>){
        try {
            val docs = om.createArrayNode()
            for (obj in objs){
                // TODO обработать ошибку и записть в шину ошибок
                //val e = prepareVKEvents(obj)
                docs.add(obj)
            }

            val req = om.createObjectNode()
            req.set("docs", docs)
            val resp = request(HttpMethod.Post, "http://localhost:5984/event_store/_bulk_docs", req)
            // TODO проверить вставку и записать ошибки в шину ошибок
        }
        catch (e: Exception){
            e.printStackTrace()
        }
    }
}

/*
        * message_new ! ok
        * message_reply ! ok
        * message_edit !
        * message_allow ! ok
        * message_deny ! ok
        *
        * group_leave !
        * group_join !
        *
        * photo_new
        * photo_comment_new !
        * photo_comment_edit !
        * photo_comment_restore !
        * photo_comment_delete !
        *
        * audio_new
        * video_new
        * video_comment_new !
        * video_comment_edit !
        * video_comment_restore !
        * video_comment_delete !
        *
        * wall_post_new
        * wall_repost
        * wall_reply_new
        * wall_reply_edit
        * wall_reply_restore
        * wall_reply_delete
        *
        * board_post_new
        * board_post_edit
        * board_post_restore
        * board_post_delete
        *
        * market_comment_new !
        * market_comment_edit !
        * market_comment_restore !
        * market_comment_delete !
        *
        * user_block
        * user_unblock
        *
        * poll_vote_new
        * group_officers_edit
        * group_change_settings
        * group_change_photo
        * vkpay_transaction
        * */