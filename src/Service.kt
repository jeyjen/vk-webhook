package polilingv

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import org.apache.commons.lang3.RandomStringUtils
import java.lang.StringBuilder
import java.time.Instant
import java.time.ZoneOffset


fun Application.service(){
    val handler = Handler()
    handler.start()
    serviceParametrized(handler)
}
fun Application.serviceParametrized(handler: Service<Event>){
    //val port = this.environment.config.property("ktor.deployment.port").getString()

    install(ContentNegotiation) {
        jackson {}
    }
    install(CORS){
        host("*")
    }
    routing {
        get("/"){
            call.respond("vk webhook")
        }

        post("/vk") {
            val code = call.request.queryParameters["code"]?: ""
            val version = call.request.queryParameters["version"]?: ""

            val rawObject = call.receive<ObjectNode>()
            rawObject.put("version", version)

            var result = "ok"
            if(code.isNullOrEmpty() || version.isNullOrEmpty()){
                result = "fail"
            }
            else{
                val event = prepareVKEvents(rawObject)
                handler.send(event)
                if (event.type == "confirmation") {
                    result = code
                }
            }
            call.respond(result)
        }
    }
}

class Event(channel: String){
    @JsonProperty("_id")
    var id = ""
    @JsonProperty("t")
    var type = ""
    @JsonProperty("v")
    var version = ""
    @JsonProperty("s")
    var source = ""
    @JsonProperty("a")
    var author = ""
    @JsonProperty("ch")
    var channel = channel
    @JsonProperty("pl")
    var payload : JsonNode? = null
}
fun prepareVKEvents(raw: JsonNode): Event{
    val e = Event("vk-group")
    val type = raw.get("type").asText()
    e.type = type
    e.version = raw.get("version").asText()
    e.id = generateEventId()
    if(
            type == "message_new" ||
            type == "message_deny" ||
            type == "message_allow" ||
            type == "group_change_photo"){
        e.source = raw.get("group_id")?.asText()?:""
        e.author = raw.get("object")?.get("user_id")?.asText() ?: ""
        e.payload = raw?.get("object")
    }
    else if(
            type == "wall_post_new" ||
            type == "message_reply" ||
            type == "confirmation"
    ) {
        e.source = raw?.get("group_id")?.asText() ?: ""
        e.author = e.source
        e.payload = raw?.get("object")
    }
    return e
}
fun generateEventId():String{
    var id = StringBuilder()
    var time = Instant.now().atOffset( ZoneOffset.UTC )

    time.year.toString()
    id.append(time.year)
    if(time.monthValue < 10)
        id.append('0')
    id.append(time.monthValue)

    if(time.dayOfMonth < 10)
        id.append('0')
    id.append(time.dayOfMonth)

    if(time.hour < 10)
        id.append('0')
    id.append(time.hour)

    if(time.minute < 10)
        id.append('0')
    id.append(time.minute)

    if(time.second < 10)
        id.append('0')
    id.append(time.second)

    if(time.nano < 10)
        id.append('0')

    id.append(time.nano)
    id.append('-')
    id.append(RandomStringUtils.randomAlphanumeric(3))

    return id.toString()
}